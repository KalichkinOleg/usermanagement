package ru.kalichkin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;

/**
 * Основной класс JschoolSpringApplication аннотируется с помощью @ SpringBootApplication,
 * который эквивалентен использованию @ Configuration , @ EnableAutoConfiguration и @ СomponentScan.
 */

@SpringBootApplication
public class JschoolSpringApplication extends SpringBootServletInitializer {
    public static void main(String[] args) {
        SpringApplication.run(JschoolSpringApplication.class, args);
    }
}
