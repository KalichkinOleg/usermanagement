package ru.kalichkin.dao;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Класс описывает общие методы для работы с базой данных, без привязки к конкретной сущности и берет на себя
 * всю сложную работу, остальные классы сущностей используют его функционал.
 */

@Transactional
public class GenericDaoImpl<T, PK extends Serializable> implements IGenericDao<T, PK> {

    @Autowired
    private SessionFactory sessionFactory;

    private Class<T> clazz;

    GenericDaoImpl(Class<T> type) {
        this.clazz = type;
    }

    /**
     * Добавление сущности в базу данных.
     */
    @Transactional
    @Override
    public void save(T obj) {
        sessionFactory.getCurrentSession().save(obj);
    }

    /**
     * Обновление сущности в базе данных.
     */
    @Transactional
    @Override
    public void update(T obj) {
        sessionFactory.getCurrentSession().update(obj);
    }

    /**
     * Получение сущности по индетификатору.
     */
    @Transactional
    @Override
    public T getById(PK id) {
        return sessionFactory.getCurrentSession().get(clazz, id);
    }

    /**
     * Получение сущности по определенному критерию.
     */
    @Transactional
    @SuppressWarnings("unchecked")
    @Override
    public List<T> findAllByMulti(Map<String, Object> condition) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(clazz);
        condition.forEach((k, v) -> {
            if (k != null) {
                criteria.add(Restrictions.eq(k, v));
            }
        });
        return (List<T>) criteria.list();
    }

    /**
     * Получение всех сущностей из базы данных.
     */
    @Transactional
    @Override
    public List<T> findAll() {
        return findAll(null);
    }

    @Transactional
    @SuppressWarnings("unchecked")
    @Override
    public List<T> findAll(Order order) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(clazz);
        if (order != null) {
            criteria.addOrder(order);
        }
        return (List<T>) criteria.list();
    }

    /**
     * Удаление сущности из базы данных по идентификатору.
     */
    @Transactional
    @Override
    public void remove(int id) {
        Object obj = sessionFactory.getCurrentSession().load(clazz, id);
        sessionFactory.getCurrentSession().delete(obj);
    }
}
