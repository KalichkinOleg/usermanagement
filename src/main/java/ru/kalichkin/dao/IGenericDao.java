package ru.kalichkin.dao;

import org.hibernate.criterion.Order;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 *  Интерфейс определеяет методы для работы с базой данных для любых сущностей.
 */

public interface IGenericDao<T, PK extends Serializable> {

    @Transactional
    void save(T obj);

    @Transactional
    void update(T obj);

    T getById(PK id);

    List<T> findAllByMulti(Map<String, Object> condition);

    @Transactional
    List<T> findAll();

    @Transactional
    List<T> findAll(Order order);

    @Transactional
    void remove(int userId);
}
