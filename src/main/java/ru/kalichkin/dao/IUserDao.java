package ru.kalichkin.dao;

import ru.kalichkin.model.User;

import java.util.List;

/**
 *  Интерфейс определяет методы для работы с базой данных для сущности User.
 */

public interface IUserDao {

    List<User> getAllUsers();

    void saveUser(User user);

    void updateUser(User user);

    List<User> findByLogin(String login);

    User userGetById(int id);

    void removeUser(int contactId);

    void removeSelected(int[] usersId);
}
