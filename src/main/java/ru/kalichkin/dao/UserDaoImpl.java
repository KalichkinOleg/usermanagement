package ru.kalichkin.dao;

import org.springframework.stereotype.Repository;
import ru.kalichkin.model.User;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Класс реализует методы описанные в интерфейсе UserDao для работы с базой данных.
 */

@Repository
public class UserDaoImpl extends GenericDaoImpl<User, Integer> implements IUserDao {

    public UserDaoImpl() {
        super(User.class);
    }

    @Override
    public List<User> getAllUsers() {
        return findAll();
    }


    @Override
    public void saveUser(User user) {
        save(user);
    }


    @Override
    public void updateUser(User user) {
        update(user);
    }


    @Override
    public List<User> findByLogin(String login) {
        Map<String, Object> condition = new HashMap<>();
        condition.put("login", login);
        return findAllByMulti(condition);
    }


    @Override
    public User userGetById(int id) {
        return getById(id);
    }


    @Override
    public void removeUser(int userId) {
        remove(userId);
    }


    @Override
    public void removeSelected(int[] usersId) {
        List<User> users = getAllUsers();
        for (User user : users) {
            for (int id : usersId) {
                if (user.getId() == id) {
                    remove(id);
                    break;
                }
            }
        }
    }
}
