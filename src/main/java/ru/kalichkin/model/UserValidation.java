package ru.kalichkin.model;


/**
 * Класс определяет валидацию состоящую из булевой переменной (есть ошибка или нет) и описании этой ошибки.
 **/

public class UserValidation {
    private String error;
    private boolean isValid;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public boolean isValid() {
        return isValid;
    }

    public void setValid(boolean valid) {
        isValid = valid;
    }
}
