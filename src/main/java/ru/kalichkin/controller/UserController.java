package ru.kalichkin.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.kalichkin.converter.UserDtoConverter;
import ru.kalichkin.dto.UserDto;
import ru.kalichkin.model.User;
import ru.kalichkin.service.UserService;

import java.util.Arrays;
import java.util.List;

/**
 *
 * @ Controller -  Данная аннотация отображает, что данный класс является контроллером
 * и в нем нужно искать методы помеченные аннотацией @RequestMapping для вызова их из Диспетчер-сервлета.
 * @ RequestMapping - Аннотация используется для маппинга урл-адреса запроса на указанный метод или класс.
 * @ Autowired - Аннотация позволяет автоматически установить значение поля, если класс находится в контейнере бинов.
 * @ ResponseBody - Аннотация показывает что данный метод возвращает объект в виде json.
 * @ RequestBody - Аннотация показывает что данный метод получает на вход объект в виде json.
 * Logger записывает все действия в Log
 */

@Controller
@RequestMapping("/api/v1/user")
public class UserController {
    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private UserDtoConverter userDtoConverter;


    @RequestMapping(value = "getAll", method = RequestMethod.GET)
    @ResponseBody
    public List<User> getAllUsers() {
        logger.info("called method \"getAllUsers\"");
        return userService.getAllUsers();
    }

    @RequestMapping(value = "create", method = RequestMethod.PUT)
    @ResponseBody
    public UserDto createUser(@RequestBody User user) {
        logger.info("called method \"createUser\": " + user.getLogin());
        return userDtoConverter.map(userService.createUser(user), user);
    }

    @RequestMapping(value = "delete", method = RequestMethod.DELETE)
    @ResponseBody
    public void deleteUser(@RequestBody int userId) {
        userService.deleteUser(userId);
        logger.info("called method \"deleteUser\" with user id: " + userId);
    }

    @RequestMapping(value = "update", method = RequestMethod.POST)
    @ResponseBody
    public UserDto updateUser(@RequestBody User user) {
        logger.info("called method \"updateUser\" with user: " + user.getLogin());
        return userDtoConverter.map(userService.updateUser(user), user);
    }

    @RequestMapping(value = "deleteSelected", method = RequestMethod.DELETE)
    @ResponseBody
    public void deleteSelectedUsers(@RequestBody int[] usersId) {
        userService.deleteSelectedUsers(usersId);
        logger.info("called method \"deleteSelectedUsers\" with several users with id: " + Arrays.toString(usersId));
    }
}



