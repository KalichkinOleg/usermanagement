package ru.kalichkin.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.kalichkin.model.ErrorInfo;

/**
 * @ ControllerAdvice - анотация которая создает глобальный перехватчик исключений.
 * Он будет перехватывать исключения, которые не указаны в контроллере.
*/

@ControllerAdvice
public class ErrorController {
    private static final Logger logger = LoggerFactory.getLogger(ErrorController.class);

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public ErrorInfo processException(Exception e) {
        logger.info("error happened", e);
        return new ErrorInfo(e.getMessage());
    }
}
