package ru.kalichkin.dto;

import ru.kalichkin.model.User;
import ru.kalichkin.model.UserValidation;

/**
 * Data Transfer Object
 */

public class UserDto {
    private UserValidation userValidation;
    private User user;

    public UserValidation getUserValidation() {
        return userValidation;
    }

    public void setUserValidation(UserValidation userValidation) {
        this.userValidation = userValidation;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}

