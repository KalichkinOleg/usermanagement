package ru.kalichkin.converter;

import org.springframework.stereotype.Component;
import ru.kalichkin.dto.UserDto;
import ru.kalichkin.model.User;
import ru.kalichkin.model.UserValidation;


/**
 * Класс конвертирует Entity в Data Transfer Object (DTO) для отправки на клиент
 */

@Component
public class UserDtoConverter {
    public UserDto map(UserValidation userValidation, User user) {
        UserDto userDto = new UserDto();

        User userFromDto = new User();
        UserValidation userValidationFromDto = new UserValidation();
        userFromDto.setId(user.getId());
        userFromDto.setFirstName(user.getFirstName());
        userFromDto.setLastName(user.getLastName());
        userFromDto.setDateOfBirth(user.getDateOfBirth());
        userFromDto.setLogin(user.getLogin());
        userFromDto.setPassword(user.getPassword());
        userFromDto.setAboutYourself(user.getAboutYourself());
        userFromDto.setResidentialAddress(user.getResidentialAddress());


        userValidationFromDto.setValid(userValidation.isValid());
        userValidationFromDto.setError(userValidation.getError());

        userDto.setUser(userFromDto);
        userDto.setUserValidation(userValidationFromDto);

        return userDto;
    }
}
