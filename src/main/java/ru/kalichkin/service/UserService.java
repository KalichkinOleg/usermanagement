package ru.kalichkin.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kalichkin.dao.IUserDao;
import ru.kalichkin.model.User;
import ru.kalichkin.model.UserValidation;

import java.util.List;

/**
 * @ Service - указывает, что класс является сервисом для реализации бизнес логики.
 */

@Service
public class UserService {

    @Autowired
    private IUserDao userDao;

    private boolean isExistUserWithLogin(String login) {
        List<User> userList = userDao.findByLogin(login);
        return !userList.isEmpty();
    }

    private UserValidation setErrorMessage(UserValidation userValidation, String message) {
        userValidation.setValid(false);
        userValidation.setError(message);
        return userValidation;
    }

    private UserValidation validateUser(User user) {
        UserValidation userValidation = new UserValidation();
        userValidation.setValid(true);

        if (user.getFirstName().isEmpty()) {
            return setErrorMessage(userValidation, "Поле Имя должно быть заполнено.");
        }

        if (user.getLastName().isEmpty()) {
           return setErrorMessage(userValidation, "Поле Фамилия должно быть заполнено.");
        }

        if (user.getLogin().isEmpty()) {
            return setErrorMessage(userValidation, "Поле Логин должно быть заполнено.");
        }

        if (user.getPassword().isEmpty()) {
           return setErrorMessage(userValidation, "Поле Пароль должно быть заполнено.");
        }


        if (isExistUserWithLogin(user.getLogin())) {
            User userInDB = userDao.userGetById(user.getId());
            if (userInDB != null && user.getLogin().equals(userInDB.getLogin())) {
                return userValidation;
            } else {
               return setErrorMessage(userValidation, "Такой логин уже существует. Попробуйте выбрать другой логин.");
            }
        }

        return userValidation;
    }

    public UserValidation createUser(User user) {
        UserValidation userValidation = validateUser(user);
        if (userValidation.isValid()) {
            userDao.saveUser(user);
        }
        return userValidation;
    }

    public List<User> getAllUsers() {
        return userDao.getAllUsers();
    }

    public void deleteUser(int userId) {
        userDao.removeUser(userId);
    }

    public UserValidation updateUser(User user) {
        UserValidation userValidation = validateUser(user);
        if (userValidation.isValid()) {
            userDao.updateUser(user);
        }
        return userValidation;
    }

    public void deleteSelectedUsers(int[] usersId) {
        userDao.removeSelected(usersId);
    }
}
