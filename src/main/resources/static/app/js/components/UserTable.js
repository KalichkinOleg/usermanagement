import React, {Component} from 'react';
import TableRow from "./TableRow";
import Checkbox from "./Checkbox";

export default class UserTable extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let isAllChecked = this.props.checked.length === 0 ? false
            : Lazy(this.props.checked).filter(c => c).toArray().length
            === this.props.checked.length;

        return (
            <table onClick={(e) => {
                this.props.users.length > 1 ? this.markedRow(e) : null
            }
            } className="table table-hover">
                <thead>
                <tr className='text-center'>
                    <th className='text-center'>
                        <label>
                            <Checkbox onChange={(event) => {
                                this.props.onSelectAllCheckbox(event)
                            }} checked={isAllChecked}/>
                        </label>
                    </th>
                    <th className='text-center'>№</th>
                    <th className='text-center'>Логин</th>
                    <th className='text-center'>Имя</th>
                    <th className='text-center'>Фамилия</th>
                </tr>
                </thead>
                <tbody>
                {
                    Lazy(this.props.users).map((user, i) =>
                        <TableRow
                            key={user.id}
                            number={i + 1}
                            user={user}
                            onDeleteUser={(id) => {
                                this.props.onDeleteUser(id);
                            }}
                            checked={this.props.checked[i]}
                            onChangeChecked={(event) => {
                                this.props.onChangeChecked(i, event)
                            }}
                            onAddCheckbox={() => {
                                this.props.onAddCheckbox(false)
                            }}
                            onVisibleUserChanged={(user) => {
                                this.props.onVisibleUserChanged(user)
                            }}
                        />).toArray()
                }
                </tbody>
            </table>
        )
    }

    markedRow(e) {
        let elem = e.target || e.srcElement;
        let row = elem.parentNode;
        let table = row.parentNode.parentNode;

        if (row.tagName === 'TR' && row.parentNode.tagName === 'TBODY') {
            row.className += "info";
            row.setAttribute('marked', true);

            let lastMarked = table.getAttribute('lastMarked');
            if (lastMarked > this.props.users.length - 1) {
                lastMarked = null;
            }

            if (lastMarked !== null && lastMarked !== '' && row.sectionRowIndex !== lastMarked) {
                let lastRow = table.tBodies[0].rows[lastMarked];
                lastRow.className = lastRow.className.replace('info', '');
                lastRow.removeAttribute('marked');
            }
            table.setAttribute("lastMarked", row.sectionRowIndex);
        }
    }
}

