import React, {Component} from 'react';
import {Button, ControlLabel, FormGroup} from "react-bootstrap";
import 'react-datetime/css/react-datetime.css'
import Datetime from "react-datetime";

export default class InputForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            lastName: '',
            firstName: '',
            dateOfBirth: '',
            login: '',
            password: '',
            aboutYourself: '',
            residentialAddress: '',
            formErrors: {lastName: '', firstName: '', login: '', password: ''},
            lastNameValid: false,
            firstNameValid: false,
            passwordValid: false,
            loginValid: false,
            formValid: false
        }
    }


    validateField(fieldName, value) {
        let fieldValidationErrors = this.state.formErrors;
        let firstNameValid = this.state.firstNameValid;
        let lastNameValid = this.state.lastNameValid;
        let loginValid = this.state.loginValid;
        let passwordValid = this.state.passwordValid;


        switch (fieldName) {
            case 'lastName':
                lastNameValid = value.match('([^\\s*]+)', 'g');
                fieldValidationErrors.lastName = lastNameValid ? '' : 'Поле "Фамилия" должно быть заполнено.';
                break;
            case 'firstName':
                firstNameValid = value.match('([^\\s*]+)', 'g');
                fieldValidationErrors.firstName = firstNameValid ? '' : 'Поле "Имя" должно быть заполнено.';
                break;
            case 'login':
                loginValid = value;
                fieldValidationErrors.login = loginValid ? '' : 'Поле "Логин" должно быть заполнено.';
                let sameLogin = Lazy(this.props.users).find({login: loginValid});
                if (sameLogin) {
                    loginValid = '';
                    fieldValidationErrors.login = 'Такой логин уже существует.'
                }
                break;
            case 'password':
                passwordValid = value.match('([^\\s*]+)', 'g');
                fieldValidationErrors.password = passwordValid ? '' : 'Поле "Пароль" должно быть заполнено.';
                break;
            default:
                break;
        }

        this.setState({
            formErrors: fieldValidationErrors,
            lastNameValid: lastNameValid,
            firstNameValid: firstNameValid,
            loginValid: loginValid,
            passwordValid: passwordValid,
        }, this.validateForm);
    }


    validateForm() {
        this.setState({
            formValid: this.state.firstNameValid && this.state.lastNameValid && this.state.loginValid
            && this.state.passwordValid
        });
    }


    static errorClass(error) {
        return (error.length === 0 ? '' : 'has-error');
    }


    handleDateInput(moment) {
        let value = moment.calendar();
        this.setState({
            dateOfBirth: value,
        });
    };

    handleUserInput(e) {
        const name = e.target.name;
        const value = e.target.value;
        this.setState({[name]: value},
            () => {
                this.validateField(name, value)
            });
    };

    render() {
        return (
            <FormGroup bsClass='form'>
                <div className={`${InputForm.errorClass(this.state.formErrors.lastName)}`}>
                    <ControlLabel bsClass='form-label'>
                        <span className='form-field'>Фамилия:</span>
                        <input name='lastName' type='text' className="form-control input-sm form-input"
                               value={this.state.lastName} onChange={(e) => {
                            this.handleUserInput(e)
                        }}/>
                        <span className="error-message"> {this.state.formErrors.lastName}</span>
                    </ControlLabel>
                </div>

                <div className={`${InputForm.errorClass(this.state.formErrors.firstName)}`}>
                    <ControlLabel bsClass='form-label'>
                        <span className='form-field'>Имя:</span>
                        <input type='text' name='firstName' className="form-control input-sm form-input"
                               value={this.state.firstName} onChange={(e) => {
                            this.handleUserInput(e)
                        }}/>
                        <span className="error-message"> {this.state.formErrors.firstName}</span>
                    </ControlLabel>
                </div>

                <div>
                    <ControlLabel bsClass='form-label'>
                        <span className='form-field'>Дата рождения:</span>
                        <Datetime locale="ru" className="form-input" dateFormat="DD.MM.YYYY"
                                  timeFormat={false}
                                  value={this.state.dateOfBirth}
                                  viewMode="years"
                                  closeOnSelect={true}
                                  onChange={(moment) => {
                                      this.handleDateInput(moment);
                                  }}
                                  inputProps={{
                                      maxLength: "0",
                                      style: {fontSize: "small", height: "35px"}
                                  }}
                        />
                    </ControlLabel>
                </div>

                <div className={`${InputForm.errorClass(this.state.formErrors.login)}`}>
                    <ControlLabel bsClass='form-label'>
                        <span className='form-field'>Логин:</span>
                        <input type='text' name='login' className="form-control input-sm form-input"
                               value={this.state.login} onChange={(e) => {
                            this.handleUserInput(e)
                        }}/>
                        <span className="error-message"> {this.state.formErrors.login}</span>
                    </ControlLabel>
                </div>

                <div className={`${InputForm.errorClass(this.state.formErrors.password)}`}>
                    <ControlLabel bsClass='form-label'>
                        <span className='form-field'>Пароль:</span>
                        <input type='password' name='password' className="form-control input-sm form-input"
                               value={this.state.password} onChange={(e) => {
                            this.handleUserInput(e)
                        }}/>
                        <span className="error-message"> {this.state.formErrors.password}</span>
                    </ControlLabel>
                </div>
                <div>
                    <ControlLabel bsClass='form-label'>
                        <span className='form-field'>Информация о себе:</span>
                        <textarea rows="3" name='aboutYourself' className="form-control text-area input-sm form-input"
                                  value={this.state.aboutYourself} onChange={(e) => {
                            this.handleUserInput(e)
                        }}/>
                    </ControlLabel>
                </div>
                <div>
                    <ControlLabel bsClass='form-label'>
                        <span className='form-field'>Адрес проживания:</span>
                        <input name='residentialAddress' type='text' className="form-control input-sm form-input"
                               value={this.state.residentialAddress} onChange={(e) => {
                            this.handleUserInput(e)
                        }}/>
                    </ControlLabel>
                </div>
                <Button type='button' bsClass='btn' bsSize="sm" bsStyle="primary" disabled={!this.state.formValid}
                        onClick={() => {
                            this.addUser()
                        }}>Добавить
                </Button>
            </FormGroup>
        )
    }

    addUser() {
        this.props.onAddUser(this.state.lastName, this.state.firstName, this.state.dateOfBirth, this.state.login,
            this.state.password, this.state.aboutYourself, this.state.residentialAddress);

        this.setState({
            lastName: '',
            firstName: '',
            dateOfBirth: '',
            login: '',
            password: '',
            aboutYourself: '',
            residentialAddress: '',
            lastNameValid: false,
            firstNameValid: false,
            passwordValid: false,
            loginValid: false,
            formValid: false
        })
    }
}
