import React, {Component} from 'react';
import Checkbox from "./Checkbox";

export default class TableRow extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.onAddCheckbox();
    }

    handleClick() {
        this.props.onVisibleUserChanged(this.props.user);
    }

    render() {
        return (
            <tr onClick={(e) => {
                this.handleClick(e);
            }}>
                <td className='text-center'>
                    <label className='check-box-table-cell'>
                        <Checkbox checked={this.props.checked}
                                  onChange={(event) => {
                                      this.props.onChangeChecked(event)
                                  }}/>
                    </label>
                </td>
                <td className='text-center'>{this.props.number}</td>
                <td className='text-center'>{this.props.user.login}</td>
                <td className='text-center'>{this.props.user.firstName}</td>
                <td className='text-center'>{this.props.user.lastName}</td>
            </tr>
        );
    }
}