import React, {Component} from 'react';
import {Button, ControlLabel} from "react-bootstrap";
import Datetime from "react-datetime";
import User from "./User";


export default class VisibleUser extends Component {
    constructor(props) {
        super(props);

        this.state = {
            update: false,
            lastName: '',
            firstName: '',
            dateOfBirth: '',
            login: '',
            password: '',
            aboutYourself: '',
            residentialAddress: '',
            formErrors: {lastName: '', firstName: '', login: '', password: ''},
            lastNameValid: true,
            firstNameValid: true,
            passwordValid: true,
            loginValid: true,
            formValid: true
        }
    }

    validateField(fieldName, value) {
        let fieldValidationErrors = this.state.formErrors;
        let firstNameValid = this.state.firstNameValid;
        let lastNameValid = this.state.lastNameValid;
        let loginValid = this.state.loginValid;
        let passwordValid = this.state.passwordValid;

        switch (fieldName) {
            case 'lastName':
                lastNameValid = value.match('([^\\s*]+)', 'g');
                fieldValidationErrors.lastName = lastNameValid ? '' : 'Поле "Фамилия" должно быть заполнено.';
                break;
            case 'firstName':
                firstNameValid = value.match('([^\\s*]+)', 'g');
                fieldValidationErrors.firstName = firstNameValid ? '' : 'Поле "Имя" должно быть заполнено.';
                break;
            case 'login':
                loginValid = value;
                fieldValidationErrors.login = loginValid ? '' : 'Поле "Логин" должно быть заполнено.';
                let sameLogin = Lazy(this.props.users).find({login: loginValid});
                if (sameLogin) {
                    if (sameLogin.login !== this.props.visibleUser.login) {
                        loginValid = '';
                        fieldValidationErrors.login = 'Такой логин уже существует.'
                    }
                }
                break;
            case 'password':
                passwordValid = value.match('([^\\s*]+)', 'g');
                fieldValidationErrors.password = passwordValid ? '' : 'Поле "Пароль" должно быть заполнено.';
                break;
            default:
                break;
        }

        this.setState({
            formErrors: fieldValidationErrors,
            lastNameValid: lastNameValid,
            firstNameValid: firstNameValid,
            loginValid: loginValid,
            passwordValid: passwordValid,
        }, this.validateForm);
    }


    validateForm() {
        this.setState({
            formValid: this.state.firstNameValid && this.state.lastNameValid && this.state.loginValid
            && this.state.passwordValid
        });
    }


    handleUpdate() {
        this.setState({
            update: !this.state.update,
            lastName: this.props.visibleUser.lastName,
            firstName: this.props.visibleUser.firstName,
            dateOfBirth: this.props.visibleUser.dateOfBirth,
            login: this.props.visibleUser.login,
            password: this.props.visibleUser.password,
            aboutYourself: this.props.visibleUser.aboutYourself,
            residentialAddress: this.props.visibleUser.residentialAddress
        })
    }

    handleDateInput(moment) {
        let value = moment.calendar();
        this.setState({
            dateOfBirth: value,
        });
    };

    handleUserInput(e) {
        const name = e.target.name;
        const value = e.target.value;
        this.setState({[name]: value},
            () => {
                this.validateField(name, value)
            })
    };

    render() {
        return (
            <div className={this.props.users.length === 0 || this.props.visibleUser.login === ''
                ? 'hidden' : 'thumbnail'}>
                <div className="visible-user">

                    {this.state.update ? <input className="visible-user-input" type='text' name="firstName"
                                                value={this.state.firstName} onChange={(e) => {
                            this.handleUserInput(e)
                        }}/> :
                        <h3 className="text-center"><b>{this.props.visibleUser.firstName}</b></h3>}


                    {this.state.update ? <input className="visible-user-input" type='text' name="lastName"
                                                value={this.state.lastName} onChange={(e) => {
                        this.handleUserInput(e)
                    }}/> : <h3 className="text-center"><b>{this.props.visibleUser.lastName}</b></h3>}

                    <table className="table table-responsive">
                        <tbody>
                        <tr>
                            <td className="td-thumbnail-head">Дата рождения:</td>
                            {this.state.update ?
                                <td><Datetime locale="ru" className="visible-user-td-input" dateFormat="DD.MM.YYYY"
                                              timeFormat={false}
                                              value={this.state.dateOfBirth}
                                              viewMode="years"
                                              closeOnSelect={true}
                                              onChange={(moment) => {
                                                  this.handleDateInput(moment);
                                              }}
                                              inputProps={{
                                                  maxLength: "0",
                                                  className: 'datetime-input'
                                              }}
                                /></td> :
                                <td>{this.props.visibleUser.dateOfBirth}</td>}
                        </tr>
                        <tr>
                            <td className="td-thumbnail-head">Логин:</td>
                            {this.state.update ?
                                <td><input className="visible-user-td-input" type='text' name="login"
                                           value={this.state.login}
                                           onChange={(e) => {
                                               this.handleUserInput(e)
                                           }}/>
                                </td> :
                                <td>{this.props.visibleUser.login}</td>}
                        </tr>
                        <tr>
                            <td className="td-thumbnail-head">Пароль:</td>
                            {this.state.update ?
                                <td><input className="visible-user-td-input" type='password' name="password"
                                           value={this.state.password}
                                           onChange={(e) => {
                                               this.handleUserInput(e)
                                           }}/>
                                </td> :
                                <td>{this.props.visibleUser.password}</td>}
                        </tr>
                        </tbody>
                    </table>
                    <p className="text-center">
                        <b>Информация о себе:</b><br/>
                        {this.state.update ?
                            <textarea name="aboutYourself" rows={3}
                                      value={this.state.aboutYourself} onChange={(e) => {
                                this.handleUserInput(e)
                            }}/> :
                            this.props.visibleUser.aboutYourself}
                    </p>
                    <p className="text-center">
                        <b>Адрес проживания:</b><br/>
                        {this.state.update ?
                            <input className="visible-user-input" type='text'
                                   value={this.state.residentialAddress} name="residentialAddress"
                                   onChange={(e) => {
                                       this.handleUserInput(e)
                                   }}/> :
                            this.props.visibleUser.residentialAddress}
                    </p>

                    <ControlLabel bsClass='form-label'>
                        <span className="error-message"> {this.state.formErrors.firstName}</span>
                        <span className="error-message"> {this.state.formErrors.lastName}</span>
                        <span className="error-message"> {this.state.formErrors.login}</span>
                        <span className="error-message"> {this.state.formErrors.password}</span>
                    </ControlLabel>

                    <div className="btn-group-visible-user">
                        <Button type='button' bsClass='btn-visible-user btn' bsSize="sm" bsStyle="danger"
                                onClick={() => {
                                    this.props.onDeleteUser(this.props.visibleUser.id)
                                }}>Удалить
                        </Button>


                        <Button type='button' bsClass='btn-visible-user btn' bsSize="sm"
                                bsStyle={this.state.update ? 'success' : 'info'}
                                disabled={this.state.update ? !this.state.formValid : false}
                                onClick={() => {
                                    {
                                        this.state.update ? this.updateUser() : null
                                    }
                                    this.handleUpdate();
                                }}>{this.state.update ? 'Сохранить' : 'Редактировать'}
                        </Button>

                    </div>
                </div>
            </div>
        );
    }

    updateUser() {
        let user = new User(this.props.visibleUser.id, this.state.lastName, this.state.firstName, this.state.dateOfBirth, this.state.login,
            this.state.password, this.state.aboutYourself, this.state.residentialAddress);
        this.props.onUpdateUser(user);
    }
}