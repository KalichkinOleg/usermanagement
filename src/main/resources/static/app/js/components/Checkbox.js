import React, {Component} from 'react';

export default class Checkbox extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <input checked={this.props.checked} type='checkbox' {...this.props}/>
        )
    }
}