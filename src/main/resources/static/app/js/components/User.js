export default class User {
    constructor(id, lastName, firstName, dateOfBirth, login, password, aboutYourself, residentialAddress, checked) {
        this.id = id;
        this.lastName = lastName;
        this.firstName = firstName;
        this.dateOfBirth = dateOfBirth;
        this.login = login;
        this.password = password;
        this.aboutYourself = aboutYourself;
        this.residentialAddress = residentialAddress;
        this.checked = checked;
    }
}
