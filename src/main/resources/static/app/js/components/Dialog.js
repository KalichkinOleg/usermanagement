import React, {Component} from 'react';
import {Button, Modal} from "react-bootstrap";

export default class Dialog extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Modal bsSize="sm" backdrop={'static'} show={this.props.show} onHide={() => {
                this.props.onCloseDialog()
            }}>
                <Modal.Header closeButton>
                    <Modal.Title><strong>{this.props.title}</strong></Modal.Title>
                </Modal.Header>
                <Modal.Body>{this.props.body}</Modal.Body>
                <Modal.Footer>
                    <Button bsClass="btn" bsStyle='primary' onClick={() => {
                        this.props.onCloseDialog()
                    }}>Отмена</Button>
                    <Button bsClass='btn' bsStyle="primary"
                            onClick={() => {
                                this.props.onClickOkDialog()
                            }}>Да</Button>
                </Modal.Footer>
            </Modal>
        );
    }
}