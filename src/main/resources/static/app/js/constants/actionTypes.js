//load users
export const FETCH_ALL_USERS = 'FETCH_ALL_USERS';
export const FETCH_ALL_USERS_SUCCESS = 'FETCH_ALL_USERS_SUCCESS';
export const FETCH_ALL_USERS_ERROR = 'FETCH_ALL_USERS_ERROR';

//users
export const ADD_USER = 'ADD_USER';
export const REMOVE_USER = 'REMOVE_USER';
export const CHECKED_USER = 'CHECKED_USER';
export const REMOVE_SELECTED_USER = 'REMOVE_SELECTED_USER';
export const CHECKED_ALL = 'CHECKED_ALL';
export const UPDATE_USER = 'UPDATE_USER';


//users find
export const FIND_USER = 'FIND_USER';

//check
export const ADD_CHECK = 'ADD_CHECK';
export const CHANGE_CHECKED = 'CHANGE_CHECKED';
export const SELECT_ALL_CHECKBOX = 'SELECT_ALL_CHECKBOX';
export const REMOVE_CHECKED = 'REMOVE_CHECKED';
export const CHECK_SIZE_CHECKED = 'CHECK_SIZE_CHECKED';

//visibleUser
export const CHANGE_VISIBLE_USER = 'CHANGE_VISIBLE_USER';


