import * as types from "../constants/actionTypes";
import Lazy from 'lazy.js/lazy';

export default function checked(state = [], action) {
    switch (action.type) {
        case types.ADD_CHECK:
            return [...state, action.payload];
            break;
        case types.REMOVE_CHECKED:
            return Lazy(state).rest().toArray();
            break;
        case types.CHANGE_CHECKED:
            return Lazy(state).map((checked, i) => {
                if (i === action.payload.index) {
                    checked = action.payload.checked;
                }
                return checked;
            }).toArray();
            break;
        case types.SELECT_ALL_CHECKBOX:
            return Lazy(state).map(() => action.payload).toArray();
            break;
        case types.CHECK_SIZE_CHECKED:
            return Lazy(state).rest(action.payload).map(() => false).toArray();
            break;
        default:
            return state;
    }
}
