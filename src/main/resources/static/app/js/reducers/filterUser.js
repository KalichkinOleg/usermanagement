import * as types from "../constants/actionTypes";

export default function filterUser(state = '', action) {
    switch (action.type) {
        case types.FIND_USER:
            return action.payload;
            break;
        default:
            return state;
    }
}
