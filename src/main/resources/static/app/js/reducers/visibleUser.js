import * as types from "../constants/actionTypes";

const initialState = {
    id: '',
    lastName: '',
    firstName: '',
    dateOfBirth: '',
    login: '',
    password: '',
    aboutYourself: '',
    residentialAddress: '',
};

export default function visibleUser(state = initialState, action) {
    switch (action.type) {
        case types.CHANGE_VISIBLE_USER:
            if (action.payload !== null) {
                return {...action.payload};
            } else {
                return initialState;
            }
            break;
        default:
            return state
    }
}