import {combineReducers} from 'redux'
import checked from "./checked";
import filterUser from "./filterUser";
import users from "./users";
import visibleUser from "./visibleUser";

export default combineReducers({
    users,
    filterUser,
    checked,
    visibleUser
})