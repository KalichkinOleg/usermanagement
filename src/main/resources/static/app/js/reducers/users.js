import * as types from "../constants/actionTypes";

export default function users(state = {
    items: [],
    isLoading: false,
    errors: {}
}, action) {
    switch (action.type) {
        case types.FETCH_ALL_USERS:
            return Object.assign({}, state, {
                isLoading: true
            });
            break;
        case types.FETCH_ALL_USERS_SUCCESS:
            return Object.assign({}, state, {
                isLoading: false,
                items: action.payload
            });
            break;
        case types.FETCH_ALL_USERS_ERROR:
            return Object.assign({}, state, {
                isLoading: false,
                errors: action.errors
            });
            break;
        case types.ADD_USER:
            return Object.assign({}, state, {
                items: [...state.items, action.payload],
                isLoading: false,
                errors: {}
            });
            break;
        case types.UPDATE_USER:
            return Object.assign({}, state, {
                items: Lazy(state.items).map(user => {
                   if (user.id === action.payload.id) {
                       return action.payload;
                   }
                   return user;
                }).toArray(),
                isLoading: false,
                errors: {}
            });
            break;
        case types.REMOVE_USER:
            return Object.assign({}, state, {
                items: Lazy(state.items).filter(user => {
                    if (user.id !== action.payload) {
                        return user;
                    }
                }).toArray(),
                isLoading: false,
                errors: {}
            });
            break;
        case types.CHECKED_USER:
            return Object.assign({}, state, {
                items: Lazy(state.items).map((user, i) => {
                    if (i === action.payload.index) {
                        user.checked = action.payload.checked;
                    }
                    return user;
                }).toArray(),
                isLoading: false,
                errors: {}
            });
            break;
        case types.CHECKED_ALL:
            return Object.assign({}, state, {
                items: Lazy(state.items).map(user => {
                    user.checked = action.payload;
                    return user;
                }).toArray(),
                isLoading: false,
                errors: {}
            });
        case types.REMOVE_SELECTED_USER:
            return Object.assign({}, state, {
                items: Lazy(state.items).filter(user => {
                    if (!Lazy(action.payload).contains(user.id)) {
                        return user;
                    }
                }).toArray(),
                isLoading: false,
                errors: {}
            });
        default:
            return state;
    }
}
