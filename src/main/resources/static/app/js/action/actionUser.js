import User from "../components/User";
import * as types from "../constants/actionTypes";
import fetch from "isomorphic-fetch";


export const getAllUsersFromServer = () => dispatch => {
    dispatch({type: types.FETCH_ALL_USERS});
    fetch('/jschool/api/v1/user/getAll', {method: 'GET'})
        .then(response =>
            response.json())
        .then((users) => {
            let userListForClient = [];
            Lazy(users).each(user => {
                let userForClient = new User(user.id, user.lastName, user.firstName,
                    user.dateOfBirth, user.login, user.password, user.aboutYourself, user.residentialAddress);
                userListForClient.push(userForClient);
            });
            dispatch({type: types.FETCH_ALL_USERS_SUCCESS, payload: userListForClient})
        }).catch(errors => dispatch({type: types.FETCH_ALL_USERS_ERROR, errors: errors}))
};


export const findUser = (event) => dispatch => {
    let searchUser = event.target.value.toLowerCase();
    dispatch({type: types.FIND_USER, payload: searchUser})
};


export const addUser = (lastName, firstName, dateOfBirth, login, password, aboutYourself, residentialAddress) => dispatch => {
    let init = {
        method: 'PUT',
        dataType: 'json',
        headers: {
            "Content-type": "application/json; charset=utf-8"
        },
        body: JSON.stringify({lastName, firstName, dateOfBirth, login, password, aboutYourself, residentialAddress})
    };

    fetch('/jschool/api/v1/user/create', init)
        .then((response) => {
            return response.json()
        })
        .then((data) => {
            if (data.userValidation.valid) {
                let userForClient = new User(data.user.id, data.user.lastName, data.user.firstName, data.user.dateOfBirth,
                    data.user.login, data.user.password, data.user.aboutYourself, data.user.residentialAddress, false);
                dispatch({type: types.ADD_USER, payload: userForClient})
            } else {
                dispatch({type: types.FETCH_ALL_USERS_ERROR, errors: alert(data.userValidation.error)})
            }
        })
};


export const updateUser = (user) => dispatch => {
    let init = {
        method: 'POST',
        dataType: 'json',
        headers: {
            "Content-type": "application/json; charset=utf-8"
        },
        body: JSON.stringify(user)
    };

    fetch('/jschool/api/v1/user/update', init)
        .then((response) => {
            return response.json()
        })
        .then((data) => {
            if (data.userValidation.valid) {
                let userForClient = new User(data.user.id, data.user.lastName, data.user.firstName, data.user.dateOfBirth,
                    data.user.login, data.user.password, data.user.aboutYourself, data.user.residentialAddress, false);
                dispatch({type: types.UPDATE_USER, payload: userForClient});
                dispatch({type: types.CHANGE_VISIBLE_USER, payload: userForClient});
            } else {
                dispatch({type: types.FETCH_ALL_USERS_ERROR, errors: alert(data.userValidation.error)})
            }
        })
};


export const deleteUser = (id, users) => dispatch => {
    let init = {
        method: 'DELETE',
        dataType: 'json',
        headers: {
            "Content-type": "application/json; charset=utf-8"
        },
        body: JSON.stringify(id)
    };

    let visibleUser;
    if (users.length - 1 === 0) {
        visibleUser = null;
    } else {
        if (users[0].id !== id) {
            visibleUser = users[0];
        } else {
            visibleUser = users[users.length - 1];
        }
    }

    fetch('/jschool/api/v1/user/delete', init)
        .then(() => {
            dispatch({type: types.REMOVE_USER, payload: id});
            dispatch({type: types.REMOVE_CHECKED});
            dispatch({type: types.CHANGE_VISIBLE_USER, payload: visibleUser});
        })
};


export const deleteSelectedUsers = (users) => dispatch => {
    let selectedUsersById = Lazy(users).filter(user => user.checked).pluck('id').toArray();

    let init = {
        method: 'DELETE',
        dataType: 'json',
        headers: {
            "Content-type": "application/json; charset=utf-8"
        },
        body: JSON.stringify(selectedUsersById)
    };

    let visibleUser;
    if (users.length - selectedUsersById.length === 0) {
        visibleUser = null;
    } else {
        visibleUser = Lazy(users).findWhere({checked: false});
    }

    fetch('/jschool/api/v1/user/deleteSelected', init).then(() => {
        dispatch({type: types.REMOVE_SELECTED_USER, payload: selectedUsersById});
        dispatch({type: types.CHECKED_ALL, payload: false});
        dispatch({type: types.CHECK_SIZE_CHECKED, payload: selectedUsersById.length});
        dispatch({type: types.CHANGE_VISIBLE_USER, payload: visibleUser})
    });
};


export const changeVisibleUser = (user) => dispatch => {
    dispatch({type: types.CHANGE_VISIBLE_USER, payload: user});
};






