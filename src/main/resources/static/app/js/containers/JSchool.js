import React, {Component} from 'react';
import '../../css/index.css';
import {connect} from 'react-redux';
import InputForm from '../components/InputForm';
import * as action from "../action/actionUser";
import {Button, Grid, Col, Row} from "react-bootstrap";
import UserTable from "../components/UserTable";
import * as actionChecked from "../action/actionChecked";
import VisibleUser from "../components/VisibleUser";
import Dialog from "../components/Dialog";


class JSchool extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showModal: false,
            title: '',
            body: '',
            remove: false,
            update: false,
            removeSelected: false,
            removeId: '',
            userUpdate: {}
        }
    };

    componentWillMount() {
        this.props.onLoadUsersFromServer();
    }


    render() {
        return (
            <Grid bsClass="content">
                <Row>
                    <Col sm={2}>
                        <Dialog show={this.state.showModal} title={this.state.title} body={this.state.body}
                                onCloseDialog={(e) => {
                                    this.closeDialog(e)
                                }} onClickOkDialog={(e) => {
                            this.clickOkDialog(e)
                        }}/>
                        <InputForm users={this.props.users}
                                   onAddUser={(lastName, firstName, dateOfBirth, login, password, aboutYourself, residentialAddress) => {
                                       this.props.onAddUser(lastName, firstName, dateOfBirth, login, password, aboutYourself, residentialAddress)
                                   }}/>
                    </Col>
                    <Col sm={6}>
                        <input type='text' placeholder='Поиск...'
                               className='form-control input-sm form-input-search'
                               onChange={(e) => {
                                   this.props.onFindUser(e)
                               }}/>

                        <Button type='button'
                                disabled={!Lazy(this.props.checked).contains(true)}
                                bsClass='btn-selected-users btn'
                                bsSize="sm"
                                bsStyle="primary" onClick={() => {
                            this.removeSelectedUsersDialog()
                        }}>Удалить выбранные
                        </Button>


                        <UserTable
                            users={this.props.users}
                            onSelectAllCheckbox={(event) => {
                                this.props.onSelectAllCheckbox(event)
                            }}
                            checked={this.props.checked}
                            onChangeChecked={(index, event) => {
                                this.props.onChangeChecked(index, event)
                            }}
                            onAddCheckbox={(checked) => {
                                this.props.onAddCheckbox(checked)
                            }}
                            onVisibleUserChanged={(user) => {
                                this.visibleUserChanged(user)
                            }}
                        />
                    </Col>
                    <Col sm={3}>
                        <VisibleUser users={this.props.users} visibleUser={this.props.visibleUser}
                                     onDeleteUser={(id) => {
                                         this.removeUserDialog(id)
                                     }}
                                     onUpdateUser={(user) => {
                                         this.updateUserDialog(user)
                                     }}/>
                    </Col>
                </Row>
            </Grid>
        )
    }

    visibleUserChanged(user) {
        this.props.onVisibleUser(user);
    }

    closeDialog() {
        this.setState({
            showModal: false,
            removeSelected: false,
            remove: false,
            updateUser: false
        });
    }


    removeSelectedUsersDialog() {
        this.setState({
            showModal: true,
            title: 'Удаление пользователей',
            body: 'Вы уверены, что хотите удалить выбранных пользователей?',
            removeSelected: true
        });
    }


    removeUserDialog(id) {
        this.setState({
            showModal: true,
            title: 'Удаление пользователя',
            body: 'Вы уверены, что хотите удалить пользователя "' + Lazy(this.props.users).findWhere({id: id}).login + '"?',
            remove: true,
            removeId: id
        });
    }


    updateUserDialog(user) {
        this.setState({
            showModal: true,
            title: 'Редактирование пользователя',
            body: 'Вы уверены, что хотите внести изменения в пользователя "' + user.login + '"?',
            update: true,
            userUpdate: user
        });
    }


    clickOkDialog() {
        if (this.state.removeSelected) {
            this.props.onDeleteSelectedUsers(this.props.users);
            this.setState({
                removeSelected: false
            })
        } else if (this.state.remove) {
            this.props.onDeleteUser(this.state.removeId, this.props.users);
            this.setState({
                remove: false,
                removeId: ''
            })
        } else if (this.state.update) {
            this.props.onUpdateUser(this.state.userUpdate);
            this.setState({
                update: false
            })
        }

        this.setState({
            showModal: false
        })
    }

}


export default connect(
    state => ({
        users: Lazy(state.users.items).filter(user => {
            let searchUser = user.lastName.toLowerCase() + user.firstName.toLowerCase() + user.dateOfBirth.toLowerCase() +
                user.login.toLowerCase() + user.aboutYourself.toLowerCase() + user.residentialAddress.toLowerCase();
            return searchUser.indexOf(state.filterUser) !== -1;
        }).toArray(),
        checked: state.checked,
        visibleUser: state.visibleUser
    }),
    dispatch => ({
        onLoadUsersFromServer: () => {
            dispatch(action.getAllUsersFromServer())
        },
        onFindUser: (event) => {
            dispatch(action.findUser(event))
        },
        onAddUser: (lastName, firstName, dateOfBirth, login, password, aboutYourself, residentialAddress) => {
            dispatch(action.addUser(lastName, firstName, dateOfBirth, login, password, aboutYourself, residentialAddress));
        },
        onDeleteUser: (id, users) => {
            dispatch(action.deleteUser(id, users))
        },
        onDeleteSelectedUsers: (users) => {
            dispatch(action.deleteSelectedUsers(users));
        },
        onAddCheckbox: (checked) => {
            dispatch(actionChecked.addCheckbox(checked))
        },
        onChangeChecked: (index, event) => {
            dispatch(actionChecked.changeChecked(index, event))
        },
        onSelectAllCheckbox: (event) => {
            dispatch(actionChecked.selectAllCheckbox(event))
        },
        onVisibleUser: (user) => {
            dispatch(action.changeVisibleUser(user))
        },
        onUpdateUser: (user) => {
            dispatch(action.updateUser(user));
        }
    })
)(JSchool)


