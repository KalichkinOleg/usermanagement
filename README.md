# README #
Это проект - JSchool управление пользователями, реализованный на следующих технологиях:

Клиентская часть: HTML, CSS, JS, React, Redux, Webpack, React-bootstrap, Bootstrap, Lazy.js, babel.
Серверная часть: Spring-framework. Связь БД и Java: Hibernate.
БД: PostgreSQL установленная на компьютере, схема "jschool", логин и пароль "postgres". 
Сборка: Spring + Maven.
Запуск: Spring + Tomcat.

Что умеет делать проет: управлять пользователями (создавать, редактировать, просматривать список и детали, удалять)

Как запустить проект: 
На компьютере должны быть установлены Tomcat и PostgreSQL; 
Необходимо создать схему c названием "jschool"; 
Из папки static в консоли вызвать команду npm install и npm run build. 
Запустить Run JschoolSpringApplication - данная команда собирает проект и запускает JSchoolSpringApplication main.


Реализованные методы RESTful API:

GET {host}/jschool/api/v1/user/getAll - получениe списка пользователей.

PUT {host}/jschool/api/v1/user/create - добавление нового пользователя в формате:
Тело запроса: {
                 "firstName": "Имя",
                 "lastName": "Фамилия",
                 "dateOfBirth": "Дата рождения(DD.MM.YYYY)",
                 "login": "логин",
                 "password": "пароль",
                 "aboutYourself": "информация о себе",
                 "residentialAddress": "адрес проживания"
              }

DELETE {host}/jschool/api/v1/user/delete - удаление пользователя. Тело запроса: номер идентификатора

DELETE {host}/jschool/api/v1/user/deleteSelected - удаление выбранных пользователей. Тело запроса: [...id]

POST {host}/jschool/api/v1/user/update - редактирование пользователя.
Тело запроса: {
                 "id": "номер идентификатора",
                 "firstName": "Имя",
                 "lastName": "Фамилия",
                 "dateOfBirth": "Дата рождения(DD.MM.YYYY)",
                 "login": "логин",
                 "password": "пароль",
                 "aboutYourself": "информация о себе",
                 "residentialAddress": "адрес проживания"
              }
